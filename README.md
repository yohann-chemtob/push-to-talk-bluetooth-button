A Bluetooth macro keyboard with only one big button build to talk on Discord. When connected to a device and the user presses the button, it activates a LED.

BOM :
- a push button 
- an [ESP32](https://en.wikipedia.org/wiki/ESP32) (I used an ESP32-CAM because it was near my hand and it has a REALLY powerfull LED build in)
- a 5v power supply (whatever you have) 

Connect the push button to the GPIO 0 and the GND, flash the code ... and your ready to talk with a fancy big button!

By the way, the macro is CTRL-' , but you can change it easly in the code.

Pictures here: https://twitter.com/ChemtobYohann/status/1334652548024840201

A really comprehensive guide here: https://www.instructables.com/DIY-Bluetooth-Macro-Keypad/

And the library used here (and also in my repo if something changes radically): https://github.com/T-vK/ESP32-BLE-Keyboard/releases


