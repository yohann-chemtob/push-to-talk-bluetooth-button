/*
Ble Push Button
by Yohann Chemtob
*/

#include <BleKeyboard.h>

const int LED_FLASH1 = 33; // The small LED on the ESP32-CAM
const int LED_FLASH2 = 4; // The powerfull LED on the ESP32-CAM

const int buttonPin = 0;   

int buttonState = 0;   
int pressactive = 0;
// First param is name
// Second is manufacturer
// Third is initial batter level
BleKeyboard bleKeyboard("Discord_Button", "YC", 66);


void setup() {
  Serial.begin(9600);
  bleKeyboard.begin();
  pinMode(LED_FLASH1, OUTPUT);
  pinMode(LED_FLASH2, OUTPUT);
  digitalWrite(LED_FLASH1, HIGH);
  digitalWrite(LED_FLASH2, LOW);
  pinMode(buttonPin, INPUT);
}

// This will hold down all the following buttons.
void sendMacroCommand(uint8_t key) {
  bleKeyboard.press(KEY_LEFT_CTRL);
  //bleKeyboard.press(KEY_LEFT_SHIFT); 
  //bleKeyboard.press(KEY_LEFT_ALT); 
  bleKeyboard.press(key);
}

void loop() {
  buttonState = digitalRead(buttonPin);
  Serial.println(buttonState);
  Serial.println(bleKeyboard.isConnected());


  if (bleKeyboard.isConnected() && !buttonState && !pressactive) {
    Serial.println("Click");
    sendMacroCommand('`');
    pressactive = 1;
    digitalWrite(LED_FLASH2, HIGH);
  }
  
  if  (pressactive && buttonState){
    digitalWrite(LED_FLASH2, LOW);
    bleKeyboard.releaseAll(); // this releases the buttons
    pressactive = 0;
  }

  delay(100);
}